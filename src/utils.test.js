// > undefined
import axios from "axios"
import { getUser } from "./utils";
jest.mock('axios');
const bob = {"name":"bob"}
const sam = {"name":"sam"}
const triveni = {"name":"triveni"}



test('should fetch users', async () => {
    axios.get.mockReturnValueOnce(bob).mockReturnValueOnce(setTimeout(() => sam, 3000)).mockReturnValueOnce(setTimeout(() => triveni, 5000));
    const user = await getUser();
    expect(user).toBe(bob)
});

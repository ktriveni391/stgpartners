import axios from 'axios';
const api = `https://api.nationalize.io/?name=`;


const get = (url) => {
    try {
      return axios.get(url);
    } catch (ex) {
      console.error("error ")
    }
  }
const getUser = async () => {
    const user = await Promise.any([get(`${api}sami`),
    get(`${api}bob`),
    get(`${api}mike`)])
    console.log("#####", user)
    return user
}

export {
  getUser, get
}
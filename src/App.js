import './App.css';
import React, { useState, useEffect } from 'react';
import { getUser } from "./utils"


function App() {
  const [user, setUser] = useState("")
// const getUser = async () => {
//     const user = await Promise.any([get(`${api}sami`),
//     get(`${api}bob`),
//     get(`${api}mike`)])
//     return user
   
// }
  useEffect(() => {
    const init = async () => {
      const user = await getUser();
      setUser(user.data || {})
    }
    init()
  }, []);

  return (
    <div className="App">
      <header className="App-header">
       
       
          <h2>Name: {(user.name || "").toUpperCase()}</h2>
          <h3>countries</h3>
          {(user.country || []).map(country => <div key = {country.country_id}>
            <div>Id :{`${country.country_id}`}</div>
            <div>Probability: {`${country.probability}`}</div>
          </div>)}
      </header>
    </div>
  );
}

export default App;
